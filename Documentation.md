# MMP

## Introduction

FG MMP is used to attribute the installs to the user acquisition campaigns, which allows to calculate the CPI. It is also used to measure the revenues of each user in order to calculate the LTV.

This tool will also allow you to attribute users to the UA campaign on which they clicked. We can then optimize the UA by calculating the CPI and optimizing it.

## Integration Steps

1) **"Install"** or **"Upload"** FG MMP plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.